package com.example.trabalhofinal;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    Button treino, historico;
    TextView manual, ola;

    private NotificationManager mNotifyManager;
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private static final int NOTIFICATION_ID = 0;
    private static final String ACTION_DISMISS_NOTIFICATION = "ACTION_DISMISS_NOTIFICATION";
    public static final int PERFIL_ACTIVITY_REQUEST_CODE = 1;
    private NotificationReceiver mReceiver = new NotificationReceiver();
    Handler handler = new Handler();


    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        createNotificationChannel();

        treino = findViewById(R.id.btn_treino);
        treino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TipoTreino.class));
            }
        });

        historico = findViewById(R.id.btn_historico);
        historico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Historico.class));
            }
        });

        manual = findViewById(R.id.txt_manual);
        manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Manual.class));
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Perfil.class);
                startActivityForResult(intent, PERFIL_ACTIVITY_REQUEST_CODE);
            }
        });

        ola = findViewById(R.id.txt_ola);
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();


        DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String userNome = documentSnapshot.getString("Nome");
                String userPeso = documentSnapshot.getString("Peso");
                String userAltura = documentSnapshot.getString("Altura");
                String userIdade = documentSnapshot.getString("Idade");
                ola.setText(getString(R.string.saudacao) + " " + userNome);

                if(userPeso == null || userAltura == null || userIdade == null){
                    sendNotification();
                    registerReceiver(mReceiver, new IntentFilter(ACTION_DISMISS_NOTIFICATION));
                }
            }
        });



        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                        Thread.sleep(1210000); //valor teste, teria que ser feito para 1 semana
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

                        DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
                        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Double tempoSemanal = documentSnapshot.getDouble("Objetivo");

                                if(tempoSemanal <= 0){
                                    final Map<String, Object> user = new HashMap<>();
                                    user.put("Objetivo", 9000000);

                                    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                                    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

                                    db.collection("Users").document(mFirebaseUser.getEmail())
                                            .update(user)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    //Log.d(TAG, "DocumentSnapshot successfully written!");
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                }
                                            });

                                    sendNotification3();
                                    registerReceiver(mReceiver, new IntentFilter(ACTION_DISMISS_NOTIFICATION));

                                }else{
                                    final Map<String, Object> user = new HashMap<>();
                                    user.put("Objetivo", 9000000);

                                    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                                    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

                                    db.collection("Users").document(mFirebaseUser.getEmail())
                                            .update(user)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    //Log.d(TAG, "DocumentSnapshot successfully written!");
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                }
                                            });

                                    sendNotification2();
                                    registerReceiver(mReceiver, new IntentFilter(ACTION_DISMISS_NOTIFICATION));
                                }


                            }
                        });

                    }
                });
            }
        }).start();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.settings:
                Intent intent = new Intent(this,SettingsActivity.class);
                startActivity(intent);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void logout(View view) {
        FirebaseAuth.getInstance().signOut(); //faz logOut
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }

    private void createNotificationChannel() {

        mNotifyManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create a NotificationChannel
            CharSequence nomeCanal = getString(R.string.channel_name);
            NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL_ID, nomeCanal, NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            mNotifyManager.createNotificationChannel(channel);
        }
    }


    private NotificationCompat.Builder getNotificationBuilder() {


        Intent notificationIntent = new Intent(this, Perfil.class);
        PendingIntent pendingNotification = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent dismissIntent = new Intent(ACTION_DISMISS_NOTIFICATION); //é criado aqui para podermos fazer swipe para dar dismiss em qualquer notificação
        PendingIntent dismissPendingIntentent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, dismissIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_person_add_black_24dp)
                .setContentTitle(getString(R.string.tituloNotifica))
                .setContentText(getString(R.string.corpoNotifica))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(true)
                .setContentIntent(pendingNotification)
                .setDeleteIntent(dismissPendingIntentent);

        return mBuilder;
    }

    public void sendNotification() {
        NotificationCompat.Builder notifyBuilder = getNotificationBuilder();

        mNotifyManager.notify(NOTIFICATION_ID, notifyBuilder.build());
    }


    private NotificationCompat.Builder getNotificationBuilder2() {


        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingNotification = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent dismissIntent = new Intent(ACTION_DISMISS_NOTIFICATION); //é criado aqui para podermos fazer swipe para dar dismiss em qualquer notificação
        PendingIntent dismissPendingIntentent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, dismissIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_porterminar)
                .setContentTitle("Objetivo")
                .setContentText("Não consegui atingir o objetivo semanal!!")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingNotification)
                .setDeleteIntent(dismissPendingIntentent);

        return mBuilder;
    }

    public void sendNotification2() {
        NotificationCompat.Builder notifyBuilder = getNotificationBuilder2();

        mNotifyManager.notify(NOTIFICATION_ID, notifyBuilder.build());
    }

    private NotificationCompat.Builder getNotificationBuilder3() {


        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingNotification = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent dismissIntent = new Intent(ACTION_DISMISS_NOTIFICATION); //é criado aqui para podermos fazer swipe para dar dismiss em qualquer notificação
        PendingIntent dismissPendingIntentent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, dismissIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_terminado)
                .setContentTitle("Objetivo")
                .setContentText("Parabéns compriu o objetivo semanal!!")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingNotification)
                .setDeleteIntent(dismissPendingIntentent);

        return mBuilder;
    }

    public void sendNotification3() {
        NotificationCompat.Builder notifyBuilder = getNotificationBuilder3();

        mNotifyManager.notify(NOTIFICATION_ID, notifyBuilder.build());
    }


    public void cancelNotification() {
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.cancel(NOTIFICATION_ID);

    }

    private class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {
                case ACTION_DISMISS_NOTIFICATION:
                    cancelNotification();
                    break;
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unregisterReceiver(mReceiver);
    }


}
