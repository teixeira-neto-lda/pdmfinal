package com.example.trabalhofinal;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ExercicioViewModel extends AndroidViewModel {

    private ExercicioRepository repository;
    private LiveData<List<Exercicio>> todosExercicios;

    public ExercicioViewModel(@NonNull Application application) {
        super(application);
        repository = new ExercicioRepository(application);
        todosExercicios = repository.getTodosExercicios();
    }

    public void insert(Exercicio exercicio) {
        repository.insert(exercicio);
    }

    public void update(Exercicio exercicio) {
        repository.update(exercicio);
    }

    public void delete(Exercicio exercicio) {
        repository.delete(exercicio);
    }

    public void deleteAllExercicios() {
        repository.deleteAllNotes();
    }

    public LiveData<List<Exercicio>> getTodosExercicios() {
        return todosExercicios;
    }
}
