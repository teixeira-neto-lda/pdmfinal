package com.example.trabalhofinal;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Openweather {

    @GET("data/2.5/weather")
    Call<WeatherResponse> getWeatherDetails(@Query("lat") String lat,
                                            @Query("lon") String lon,
                                            @Query("appid") String appid,
                                            @Query("units") String units);
}
