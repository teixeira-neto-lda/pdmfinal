package com.example.trabalhofinal;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = Exercicio.class, version = 4)
public abstract class ExercicioDatabase extends RoomDatabase {

    private static ExercicioDatabase INSTANCE;

    public abstract ExercicioDao exercicioDao();

    public static synchronized ExercicioDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    ExercicioDatabase.class, "exercicio_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private ExercicioDao exercicioDao;

        private PopulateDbAsync(ExercicioDatabase db){
            exercicioDao = db.exercicioDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
