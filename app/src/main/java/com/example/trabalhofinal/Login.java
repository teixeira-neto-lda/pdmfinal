package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    EditText emailLog, passwordLog;
    Button btnLogin;
    TextView registoLink;
    FirebaseAuth fAuth;
    ProgressBar progressBarLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailLog = findViewById(R.id.loginEmail);
        passwordLog = findViewById(R.id.loginPass);
        btnLogin = findViewById(R.id.btnLogin);
        registoLink = findViewById(R.id.linkCriar);

        fAuth = FirebaseAuth.getInstance();

        progressBarLog = findViewById(R.id.progressBarLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailLog.getText().toString().trim();
                String password = passwordLog.getText().toString().trim();



                if(TextUtils.isEmpty(email)){
                    emailLog.setError(getText(R.string.semEmailInserido));
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    emailLog.setError(getText(R.string.semPasswordInserido));
                    return;
                }



                progressBarLog.setVisibility(View.VISIBLE);

                //autenticar o utilizador na firebase
                fAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(Login.this, getText(R.string.entrouSucesso), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        }else{
                            Toast.makeText(Login.this, "Erro!!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBarLog.setVisibility(View.GONE);
                        }
                    }
                });

            }
        });

        registoLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Registo.class));
            }
        });
    }
}
