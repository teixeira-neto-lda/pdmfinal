package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Historico extends AppCompatActivity {

    RecyclerView mRecyclerView;
    private ExercicioViewModel exercicioViewModel;
    private List<Exercicio> mExercicios = new ArrayList<>();
    private ExercicioAdapter.onItemClickListener itemClickListener;
    AlertDialog.Builder dialog;
    ClickHist clickHist;

    public static final int  REQUEST_DETALHES= 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);


        final ExercicioAdapter adapter = new ExercicioAdapter(/*mExercicios, itemClickListener*/);
        mRecyclerView.setAdapter(adapter);

        //o android sabe quando tem que criar um novo ou quando ja existe
        //o this serve para o viewModel saber qual lifecycle tem que ser scoped to
        //quando a atividade acabar o systema sabe que tem que destruir
        exercicioViewModel = ViewModelProviders.of(Historico.this).get(ExercicioViewModel.class);
        exercicioViewModel.getTodosExercicios().observe(this, new Observer<List<Exercicio>>() {
            @Override
            public void onChanged(List<Exercicio> exercicios) {
                //update RecycleView data

                adapter.setExercicios(exercicios);

            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
                dialog = new AlertDialog.Builder(Historico.this);
                dialog.setTitle(getText(R.string.elimMessageTitulo));
                dialog.setMessage(getText(R.string.elimMessage));
                dialog.setPositiveButton(R.string.btnPos, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exercicioViewModel.delete(adapter.getExercicioAt(viewHolder.getAdapterPosition()));
                        Toast.makeText(Historico.this, getText(R.string.exercicioApagado), Toast.LENGTH_LONG).show();
                    }
                });
                dialog.setNegativeButton(R.string.btnNeg, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    }
                });
                dialog.setCancelable(false);
                dialog.show();
            }
        }).attachToRecyclerView(mRecyclerView);
        //onExercicioClick(adapter.getExercicioAt(viewHolder.getAdapterPosition()));
        adapter.setOnItemClickListener(new ExercicioAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Exercicio exercicio) {
                Intent intent = new Intent(Historico.this, TreinoHistorico.class);

                Boolean accao = true;


                Bundle bundle = new Bundle();

                bundle.putInt(TreinoHistorico.EXTRA_ID, exercicio.getId());
                bundle.putString(TreinoHistorico.EXTRA_TIPO, exercicio.getTipo());
                bundle.putString(TreinoHistorico.EXTRA_DATA, exercicio.getData());
                bundle.putFloat(TreinoHistorico.EXTRA_CORNOMETRO, exercicio.getCornometro());
                bundle.putFloat(TreinoHistorico.EXTRA_VELOCIDADE, exercicio.getVelocidade());
                bundle.putFloat(TreinoHistorico.EXTRA_DISTANCIA, exercicio.getDistancia());
                bundle.putFloat(TreinoHistorico.EXTRA_CALORIAS, exercicio.getCalorias());
                bundle.putString(TreinoHistorico.EXTRA_ROTA, exercicio.getRota());
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_DETALHES);
            }
        });


    }

}
