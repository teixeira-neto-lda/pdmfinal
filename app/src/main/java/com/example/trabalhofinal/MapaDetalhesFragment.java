package com.example.trabalhofinal;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapaDetalhesFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private float zoom = 17;
    ArrayList<LatLng> latLongs = new ArrayList<>();


    public MapaDetalhesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mapa_detalhes, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        try {
            String rota = getArguments().getString("Detalhes");

            String[] novoArray = rota.split(";");
            for (int i = 0; i < novoArray.length; i++) {
                String[] splitLocation = novoArray[i].split(",");
                String latString = splitLocation[0];
                String longString = splitLocation[1];
                float lat = Float.parseFloat(latString);
                float lon = Float.parseFloat(longString);
                LatLng latLng = new LatLng(lat, lon);
                latLongs.add(latLng);
            }

            for (int j = 0; j < latLongs.size(); j++) {

                //Toast.makeText(getContext(), "" + latLongs.get(j), Toast.LENGTH_SHORT).show();
                if (latLongs.get(j + 1) != null) {
                    PolylineOptions polyLineOptions = new PolylineOptions();
                    polyLineOptions.add(latLongs.get(j), latLongs.get(j + 1));
                    polyLineOptions.width(24);
                    polyLineOptions.color(Color.GREEN);
                    mMap.addPolyline(polyLineOptions);
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latLongs.get(0).latitude, latLongs.get(0).longitude), zoom));
                mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLongs.get(0)).title("Start"));
                mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLongs.get(latLongs.size() -1)).title("Finish"));
            }




        } catch (Exception e) {

        }



    }

}
