package com.example.trabalhofinal;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ExercicioDao {

    @Insert
    void insert(Exercicio exercicio);

    @Update
    void update(Exercicio exercicio);

    @Delete
    void delete(Exercicio exercicio);

    @Query("DELETE FROM exercicios_table")
    void deleteAllExercicios();

    @Query("SELECT * FROM exercicios_table WHERE email = :email ORDER BY data DESC") //para ir so buscar os exercicios do user
    LiveData<List<Exercicio>> getAll(String email);

    @Query("SELECT id FROM Exercicios_table ORDER BY id DESC LIMIT 1")
    LiveData<List<Integer>> getLast();

}
