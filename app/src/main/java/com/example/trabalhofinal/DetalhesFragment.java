package com.example.trabalhofinal;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalhesFragment extends Fragment {

    ExercicioViewModel exercicioViewModel;

    public static final String EXTRA_ID_DETALHES = "EXTRA_ID_DETALHES ";
    public static final String EXTRA_TIPO_DETALHES = "EXTRA_TIPO_DETALHES ";
    public static final String EXTRA_DATA_DETALHES = "EXTRA_DATA_DETALHES ";
    public static final String EXTRA_CORNOMETRO_DETALHES = "EXTRA_CORNOMETRO_DETALHES ";
    public static final String EXTRA_VELOCIDADE_DETALHES = "EXTRA_VELOCIDADE_DETALHES ";
    public static final String EXTRA_DISTANCIA_DETALHES = "EXTRA_DISTANCIA_DETALHES ";
    public static final String EXTRA_CALORIAS_DETALHES = "EXTRA_CALORIAS_DETALHES ";
    public static final String EXTRA_ROTA_DETALHES = "EXTRA_ROTA_DETALHES ";
    public static final String EXTRA_EMAIL_DETALHES = "EXTRA_EMAIL_DETALHES ";
    public static final String DETALHES_DETALHES = "DETALHES_DETALHES ";
    Context context;
    String rotaValor;
    ClickHist clickHist;
    Button butao;

    private TextView tipoTreino, data, tempo, velocidade, distancia, calorias;

    public DetalhesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        try {
            clickHist = (ClickHist) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalhes, container, false);

        tipoTreino = v.findViewById(R.id.detalhes_tipo_treino);
        data = v.findViewById(R.id.detalhes_data);
        tempo = v.findViewById(R.id.detalhes_tempo);
        velocidade = v.findViewById(R.id.detalhes_velocidade);
        distancia = v.findViewById(R.id.detalhes_distancia);
        calorias = v.findViewById(R.id.detalhes_calorias);
        butao = v.findViewById(R.id.buttonRota);


        try {
            if (getArguments().containsKey(EXTRA_TIPO_DETALHES)) {
                float tempoValor = getArguments().getFloat(EXTRA_CORNOMETRO_DETALHES);
                float velocidadeValor = getArguments().getFloat(EXTRA_VELOCIDADE_DETALHES);
                float distanciaValor = getArguments().getFloat(EXTRA_DISTANCIA_DETALHES);
                float caloriasValor = getArguments().getFloat(EXTRA_CALORIAS_DETALHES);
                rotaValor = getArguments().getString(EXTRA_ROTA_DETALHES);






                int h = (int) (tempoValor / 3600000);
                int m = (int) (tempoValor - h * 3600000) / 60000;
                int s = (int) (tempoValor - h * 3600000 - m * 60000) / 1000;


                tipoTreino.setText(getArguments().getString(EXTRA_TIPO_DETALHES));

                if(getArguments().getString(EXTRA_TIPO_DETALHES).equals(getText(R.string.tipoCorridaGuardarExterior))){
                    butao.setVisibility(View.VISIBLE);
                }else{
                    butao.setVisibility(View.INVISIBLE);
                }

                data.setText(getArguments().getString(EXTRA_DATA_DETALHES));
                tempo.setText(String.format("%02d", h) + ":" + String.format("%02d", m) + ":" + String.format("%02d", s));
                velocidade.setText(String.format("%.2f", velocidadeValor));
                distancia.setText(String.format("%.2f", distanciaValor));
                calorias.setText(String.format("%.0f", caloriasValor));
            }
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("Error " + e.getMessage());
        }

        butao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickHist.aberturaMapa(rotaValor);
            }
        });

        return v;
    }

}
