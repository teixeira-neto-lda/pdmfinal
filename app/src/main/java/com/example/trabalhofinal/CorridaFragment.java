package com.example.trabalhofinal;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.CountDownTimer;
import android.os.SystemClock;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CorridaFragment extends Fragment implements SensorEventListener {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    private static final int REQUEST_LOCATION_PERMISSION = 101;

    private boolean tabletMode;
    private Context context;
    private Chronometer cronometro;
    private TextView velocimetro, count;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location myLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private boolean aFuncionar = false;
    private long pauseOffset, pauseOffset2, tempoParado;
    private Button iniciarCorrida, pausarCorrida, acabarCorrida, startMusica, stopMusica;
    boolean activityRunning = false;
    long tempoTotal = 0; // usado para saber o tempo de duraçao do exercicio sem contar pausas
    TextView calorias, dia;
    private long steps = 0;
    float distance = 0;
    float caloriasValor, retirarTempo;
    String intensidade;
    Exercicio exercicio;
    ExercicioViewModel exercicioViewModel;
    AlertDialog.Builder dialog;
    String rotaString = new String();
    BroadcastReceiver broadcastReceiver;
    Double tempoSemanal;

    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

    private Click clickListener;

    SensorManager sManager;
    Sensor stepSensor;


    public CorridaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        try {
            clickListener = (Click) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    public void setNotificationButtonState(Boolean isRunEnable,
                                           Boolean isPauseEnable,
                                           Boolean isStopEnable) {
        iniciarCorrida.setEnabled(isRunEnable);
        pausarCorrida.setEnabled(isPauseEnable);
        acabarCorrida.setEnabled(isStopEnable);
    }

    public void setMusicButtonState(Boolean isPlaying,
                                    Boolean isStopEnable) {
        startMusica.setEnabled(isPlaying);
        stopMusica.setEnabled(isStopEnable);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_corrida, container, false);

        sManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        cronometro = v.findViewById(R.id.cronometro);
        velocimetro = v.findViewById(R.id.velocimetro);
        count = v.findViewById(R.id.distancia);
        calorias = v.findViewById(R.id.calorias);
        startMusica = v.findViewById(R.id.startMusica);
        stopMusica = v.findViewById(R.id.stopMusica);

        dia = v.findViewById(R.id.diaID);

        exercicioViewModel = ViewModelProviders.of(getActivity()).get(ExercicioViewModel.class);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        startMusica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMusicButtonState(false, true);

                ((Corrida) getActivity()).startService(v);
            }
        });

        stopMusica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMusicButtonState(true, false);
                ((Corrida) getActivity()).stopService(v);
            }
        });

        if (aFuncionar == true) {
            startLocationUpdates();
            //long coisoTeste = pauseOffset+pauseOffset2;

            cronometro.setBase(SystemClock.elapsedRealtime() - pauseOffset2);
            distance = getDistanceRun(steps);
            cronometro.start();
        }


        if (stepSensor != null) {
            sManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(getContext(), getText(R.string.erroSensor), Toast.LENGTH_LONG).show();
        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                float nCurrentSpeed = location.getSpeed() * 3.6f;
                                velocimetro.setText(String.format("%.2f", nCurrentSpeed) + " km");
                            } else {
                                velocimetro.setText("0,00 km/h");

                            }
                        }
                    });
                }
            }
        };


        iniciarCorrida = v.findViewById(R.id.startCorrida);
        iniciarCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!aFuncionar) {
                    cronometro.setBase(SystemClock.elapsedRealtime() - pauseOffset);
                    cronometro.start();
                    aFuncionar = true;
                    activityRunning = true;


                    String currentDateTimeString = java.text.DateFormat.getDateInstance().format(new Date());//getDateTimeInstance().format(new Date());
                    dia.setText(currentDateTimeString);
                    if (steps != 0) {
                        steps = 0;
                    }
                    ((Corrida) getActivity()).startServiceMapa(v); //estamos a chamar um metodo da activity
                    startLocationUpdates();

                    broadcastReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {

                            String s = intent.getExtras().getString("string");
                            rotaString = s;
                        }
                    };
                    getActivity().registerReceiver(broadcastReceiver, new IntentFilter("passaLista"));


                   /* BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            //Toast.makeText(getContext(), "Ola "+intent.getExtras().get("speed"), Toast.LENGTH_SHORT).show();

                            velocimetro.setText(String.format("%.2f", intent.getExtras().get("speed")) + " km/h");

                        }
                    };
                    getActivity().registerReceiver(broadcastReceiver, new IntentFilter("passaSpeed"));*/


                    if (v.findViewById(R.id.corridaframe) == null) {
                        tabletMode = true;
                    } else {
                        tabletMode = false;
                    }

                    if (!tabletMode) {
                        clickListener.abreMapa(aFuncionar);
                    }

                }

                new CountDownTimer(1000, 10) { //Set Timer for 5 seconds
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {
                        setNotificationButtonState(false, true, true);
                    }
                }.start();

            }
        });

        pausarCorrida = v.findViewById(R.id.pauseCorrida);
        pausarCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNotificationButtonState(true, false, true);
                if (aFuncionar) {
                    cronometro.stop();
                    pauseOffset = SystemClock.elapsedRealtime() - cronometro.getBase();
                    aFuncionar = false;
                    stopLocationUpdates();
                    ((Corrida) getActivity()).stopServiceMapa(v);
                    distance = getDistanceRun(steps);
                    activityRunning = false;
                }

                // preciso fazer com que a contagem de passos pare mas depois quando reenicia iniciar no valor que ca estava
            }
        });

        acabarCorrida = v.findViewById(R.id.stopCorrida);
        acabarCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNotificationButtonState(true, false, false);
                if (aFuncionar) {
                    tempoTotal = SystemClock.elapsedRealtime() - cronometro.getBase();
                } else {
                    tempoTotal = pauseOffset;
                }

                DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        tempoSemanal = documentSnapshot.getDouble("Objetivo");
                    }
                });
                stopLocationUpdates();
                ((Corrida) getActivity()).stopServiceMapa(v);
                //long tempoFinal = tempoTotal;
                int h = (int) (tempoTotal / 3600000);
                int m = (int) (tempoTotal - h * 3600000) / 60000;
                int s = (int) (tempoTotal - h * 3600000 - m * 60000) / 1000;

                //float horaCalc = (float) (tempoTotal / 2.7777777777778E-7); // tentativa triste de calcular velocidade media
                cronometro.stop();
                cronometro.setBase(SystemClock.elapsedRealtime());
                pauseOffset = 0;
                aFuncionar = false;
                ((Corrida) getActivity()).stopServiceMapa(v);
                activityRunning = false;


                //Toast.makeText(context, , Toast.LENGTH_SHORT).show();
                //aqui gravamos o tempo na base de dados
                float vlociMediaMS = getDistanceRun(steps) * 1000 / s;
                final double vlociMediaKmH = (float) vlociMediaMS * 3.6;

                if (getDistanceRun(steps) == 0) {
                    //Toast.makeText(context, "0,00 km", Toast.LENGTH_SHORT).show();
                }

                dialog = new AlertDialog.Builder(context);
                dialog.setTitle(getText(R.string.pEsforcoTitulo));


                String[] items = {(String) getText(R.string.treinoLeveDialog), (String) getText(R.string.treinoModerado), (String) getText(R.string.treinoIntensoDialog)};
                int checkedItem = 1;
                dialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
                        switch (which) {
                            case 0:
                                retirarTempo = tempoTotal * (float) 0.5;
                                //intensidade = (String) getText(R.string.treinoLeve);


                                //if(tempoSemanal > 0){
                                final Map<String, Object> posTreino = new HashMap<>();
                                posTreino.put("Objetivo", tempoSemanal - retirarTempo);

                                db.collection("Users").document(mFirebaseUser.getEmail())
                                        .update(posTreino)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });

                                String tipo = (String) getText(R.string.tipoCorridaGuardarExterior);
                                String currentDateTimeString = java.text.DateFormat.getDateInstance().format(new Date());
                                exercicio = new Exercicio(mFirebaseUser.getEmail(), tipo, currentDateTimeString, tempoTotal, (float) vlociMediaKmH, getDistanceRun(steps), caloriasValor, (String) getText(R.string.treinoLeve), rotaString); //nao sei se esta a receber bem as calorias
                                exercicioViewModel.insert(exercicio);
                                // resetar as calorias
                                caloriasValor = 0;
                                calorias.setText("0 Kcal");
                                steps = 0;
                                count.setText(String.format("%.2f", getDistanceRun(steps)) + " km");
                                dialog.dismiss();
                                break;
                            case 1:
                                retirarTempo = tempoTotal * (float) 1;
                                //intensidade = (String) getText(R.string.treinoModerado);

                                final Map<String, Object> posTreino2 = new HashMap<>();
                                posTreino2.put("Objetivo", tempoSemanal - retirarTempo);

                                db.collection("Users").document(mFirebaseUser.getEmail())
                                        .update(posTreino2)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });

                                String tipo2 = (String) getText(R.string.tipoCorridaGuardarExterior);
                                String currentDateTimeString2 = java.text.DateFormat.getDateInstance().format(new Date());
                                exercicio = new Exercicio(mFirebaseUser.getEmail(), tipo2, currentDateTimeString2, tempoTotal, (float) vlociMediaKmH, getDistanceRun(steps), caloriasValor, (String) getText(R.string.treinoModerado), rotaString);
                                exercicioViewModel.insert(exercicio);
                                // resetar as calorias
                                caloriasValor = 0;
                                calorias.setText("0 Kcal");
                                steps = 0;
                                count.setText(String.format("%.2f", getDistanceRun(steps)) + " km");
                                dialog.dismiss();
                                break;
                            case 2:
                                retirarTempo = tempoTotal * (float) 2;
                                //intensidade = (String) getText(R.string.treinoIntenso);

                                final Map<String, Object> posTreino3 = new HashMap<>();
                                posTreino3.put("Objetivo", tempoSemanal - retirarTempo);

                                db.collection("Users").document(mFirebaseUser.getEmail())
                                        .update(posTreino3)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });

                                String tipo3 = (String) getText(R.string.tipoCorridaGuardarExterior);
                                String currentDateTimeString3 = java.text.DateFormat.getDateInstance().format(new Date());
                                exercicio = new Exercicio(mFirebaseUser.getEmail(), tipo3, currentDateTimeString3, tempoTotal, (float) vlociMediaKmH, getDistanceRun(steps), caloriasValor, (String) getText(R.string.treinoIntenso), rotaString); //nao sei se esta a receber bem as calorias
                                exercicioViewModel.insert(exercicio);
                                // resetar as calorias
                                caloriasValor = 0;
                                calorias.setText("0 Kcal");
                                steps = 0;
                                count.setText(String.format("%.2f", getDistanceRun(steps)) + " km");
                                rotaString = "";
                                dialog.dismiss();
                                break;
                        }
                    }
                });
                dialog.setCancelable(false);
                dialog.show();

                getActivity().unregisterReceiver(broadcastReceiver);
                //((Corrida) getActivity()).acabar();


            }
        });
        final TextView openMap = v.findViewById(R.id.txt_openMap);
        openMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseOffset2 = SystemClock.elapsedRealtime() - cronometro.getBase();
                clickListener.abreMapa(aFuncionar);
            }
        });

        //fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(v.getContext());
        if (aFuncionar != false) {
            setNotificationButtonState(false, true, true);

        } else {
            setNotificationButtonState(true, false, false);
        }

        setMusicButtonState(true, false);
        return v;
    }


    public void onLocationChanged(Location location) {
        if (location == null) {

            velocimetro.setText("0,00 km/h");
        } else {
            float nCurrentSpeed = location.getSpeed() * 3.6f;
            velocimetro.setText(String.format("%.2f", nCurrentSpeed) + " km/h");
        }
    }

    public void startLocationUpdates() {
        //verificar premissoes
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(5000); //default estava 5000
            mLocationRequest.setFastestInterval(5000);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    if (locationResult == null) {
                        return;
                    }
                    myLocation = locationResult.getLastLocation();
                    onLocationChanged(myLocation);
                    //guardar o myLocation num array
                }
            };
            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }
    }

    public void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        velocimetro.setText("0,00 km/h");
    }


    @Override
    public void onResume() {
        super.onResume();
        if (aFuncionar == true) {
            startLocationUpdates();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopLocationUpdates();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        /*float[] values = event.values;

        int value = -1;

        if (values.length > 0) {
            value = (int) values[0];
        }*/


        if (sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            steps++;
        }

        if (activityRunning) {
            count.setText(String.format("%.2f", getDistanceRun(steps)) + " km");


            DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    String userPeso = documentSnapshot.getString("Peso");
                    try {
                        caloriasValor += (float) (Float.parseFloat(userPeso) * getDistanceRun(steps) * 0.0175);
                        calorias.setText(String.format("%.0f", caloriasValor) + " Kcal");
                    } catch (Exception e) {
                        // This will catch any exception, because they are all descended from Exception
                        System.out.println("Error " + e.getMessage());
                    }
                }
            });


        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public float getDistanceRun(float distancia) {
        //distance += distance;
        distance = (distancia * 78) / (float) 100000;
        return distance;
    }


}
