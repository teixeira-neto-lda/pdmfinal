package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Gym extends AppCompatActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    private boolean tabletMode;
    private Chronometer cronometro;
    private boolean aFuncionar = false;
    private long pauseOffset;
    private Button iniciarCorrida, pausarCorrida, acabarCorrida, startMusica, stopMusica, btnTerminar;
    long tempoTotal = 0; // usado para saber o tempo de duraçao do exercicio sem contar pausas
    TextView dia;
    Float retirarTempo, retirardistancia, retirarvelocidade,tempocorrida, caloriakm,m;
    int h;
    String retirartitulo,rota;
    Exercicio exercicio;
    float caloriasValor;
    ExercicioViewModel exercicioViewModel;
    AlertDialog.Builder dialog;
    EditText titulo,distancia;
    Double tempoSemanal;

    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym);

        cronometro = findViewById(R.id.cronometro);
        startMusica = findViewById(R.id.startMusica);
        stopMusica = findViewById(R.id.stopMusica);
        dia = findViewById(R.id.diaID);
        titulo = findViewById(R.id.editTitulo);
        titulo.setFocusableInTouchMode(false);
        distancia = findViewById(R.id.editDistancia);
        distancia.setFocusableInTouchMode(false);

        exercicioViewModel = ViewModelProviders.of(Gym.this).get(ExercicioViewModel.class);

        startMusica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMusicButtonState(false, true);
                startService(new Intent(getApplicationContext(), SoundService.class));
            }
        });
        stopMusica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMusicButtonState(true, false);
                stopService(new Intent(getApplicationContext(), SoundService.class));
            }
        });
        iniciarCorrida = findViewById(R.id.startCorrida);
        iniciarCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNotificationButtonState(false, true, true);
                if (!aFuncionar) {
                    cronometro.setBase(SystemClock.elapsedRealtime() - pauseOffset);
                    cronometro.start();
                    aFuncionar = true;

                    String currentDateTimeString = java.text.DateFormat.getDateInstance().format(new Date());//getDateTimeInstance().format(new Date());
                    dia.setText(currentDateTimeString);

                    if (v.findViewById(R.id.corridaframe) == null) {
                        tabletMode = true;
                    } else {
                        tabletMode = false;
                    }
                }
            }
        });
        pausarCorrida = findViewById(R.id.pauseCorrida);
        pausarCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNotificationButtonState(true, false, true);
                if (aFuncionar) {
                    cronometro.stop();
                    pauseOffset = SystemClock.elapsedRealtime() - cronometro.getBase();
                    aFuncionar = false;

                    // preciso fazer com que a contagem de passos pare mas depois quando reenicia iniciar no valor que ca estava
                }
            }
        });
        acabarCorrida = findViewById(R.id.stopCorrida);
        acabarCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNotificationButtonState(true, false, false);
                titulo.setFocusableInTouchMode(true);
                distancia.setFocusableInTouchMode(true);
                if (aFuncionar) {
                    tempoTotal = SystemClock.elapsedRealtime() - cronometro.getBase();
                } else {
                    tempoTotal = pauseOffset;
                }

                cronometro.stop();
                //cronometro.setBase(SystemClock.elapsedRealtime());
                pauseOffset = 0;
                aFuncionar = false;
                setGuardarButtonState(true);

            }
        });
        btnTerminar = findViewById((R.id.btnTerminar));
        btnTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new AlertDialog.Builder(Gym.this);
                dialog.setTitle(getText(R.string.pEsforcoTitulo));

                DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        tempoSemanal = documentSnapshot.getDouble("Objetivo");
                    }
                });
                // Set up the input
                String[] items = {(String) getText(R.string.treinoLeveDialog), (String) getText(R.string.treinoModerado), (String) getText(R.string.treinoIntensoDialog)};
                int checkedItem = 1;
                dialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                retirarTempo = tempoTotal * (float) 0.5;
                                tempocorrida = (float) tempoTotal/3600000;
                                h = (int) (tempoTotal / 3600000);
                                m = (float) (tempoTotal - h * 3600000) / 60000;

                                if(distancia.getText().toString().matches("")){
                                    retirardistancia = Float.valueOf(0);
                                    retirarvelocidade = Float.valueOf(0);
                                }else{
                                    retirardistancia = (float) Float.valueOf(distancia.getText().toString());
                                    retirarvelocidade = (float) retirardistancia/tempocorrida;
                                }
                                if(titulo.getText().toString().matches("")){
                                    retirartitulo = (String) getText(R.string.defaultTreinoGym);
                                    Toast.makeText(Gym.this,"" + retirartitulo,Toast.LENGTH_SHORT).show();
                                } else{
                                    retirartitulo = titulo.getText().toString();
                                }

                                final Map<String, Object> posTreino = new HashMap<>();
                                posTreino.put("Objetivo", tempoSemanal - retirarTempo);

                                db.collection("Users").document(mFirebaseUser.getEmail())
                                        .update(posTreino)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });

                                cronometro.setBase(SystemClock.elapsedRealtime());
                                //intensidade = (String) getText(R.string.treinoLeve);
                                DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
                                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String userPeso = documentSnapshot.getString("Peso");
                                        try {
                                            caloriasValor = (float) (Float.parseFloat(userPeso) * 0.0175 * retirarvelocidade);
                                            caloriakm = caloriasValor * m;
                                            String currentDateTimeString = java.text.DateFormat.getDateInstance().format(new Date());
                                            exercicio = new Exercicio(mFirebaseUser.getEmail(),retirartitulo, currentDateTimeString, tempoTotal, retirarvelocidade , retirardistancia, caloriakm , (String) getText(R.string.treinoLeve),rota); //nao sei se esta a receber bem as calorias
                                            exercicioViewModel.insert(exercicio);
                                        } catch (Exception e) {
                                            // This will catch any exception, because they are all descended from Exception
                                            String currentDateTimeString = java.text.DateFormat.getDateInstance().format(new Date());
                                            exercicio = new Exercicio(mFirebaseUser.getEmail(),retirartitulo, currentDateTimeString, tempoTotal, retirarvelocidade , retirardistancia, caloriakm, (String) getText(R.string.treinoLeve),rota); //nao sei se esta a receber bem as calorias
                                            exercicioViewModel.insert(exercicio);
                                        }
                                    }
                                });
                                dialog.dismiss();
                                break;
                            case 1:
                                retirarTempo = tempoTotal * (float) 1;
                                tempocorrida = (float) tempoTotal/3600000;
                                h = (int) (tempoTotal / 3600000);
                                m = (float) (tempoTotal - h * 3600000) / 60000;

                                if(distancia.getText().toString().matches("") ){
                                    retirardistancia = Float.valueOf(0);
                                    retirarvelocidade = Float.valueOf(0);
                                }else{
                                    retirardistancia = Float.valueOf(distancia.getText().toString());
                                    retirarvelocidade = (float) retirardistancia/tempocorrida;
                                }
                                if(titulo.getText().toString().matches("")){
                                    retirartitulo = (String) getText(R.string.defaultTreinoGym);
                                } else{
                                    retirartitulo = titulo.getText().toString();
                                }

                                final Map<String, Object> posTreino2 = new HashMap<>();
                                posTreino2.put("Objetivo", tempoSemanal - retirarTempo);

                                db.collection("Users").document(mFirebaseUser.getEmail())
                                        .update(posTreino2)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });

                                cronometro.setBase(SystemClock.elapsedRealtime());
                                //intensidade = (String) getText(R.string.treinoLeve);
                                DocumentReference docRef2 = db.collection("Users").document(mFirebaseUser.getEmail());
                                docRef2.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String userPeso = documentSnapshot.getString("Peso");
                                        try {
                                            caloriasValor = (float) (Float.parseFloat(userPeso) * 0.0175 * retirarvelocidade);
                                            caloriakm = caloriasValor * m;
                                            String currentDateTimeString2 = java.text.DateFormat.getDateInstance().format(new Date());
                                            exercicio = new Exercicio(mFirebaseUser.getEmail(),retirartitulo, currentDateTimeString2, tempoTotal, retirarvelocidade , retirardistancia, caloriakm , (String) getText(R.string.treinoModerado),rota); //nao sei se esta a receber bem as calorias
                                            exercicioViewModel.insert(exercicio);
                                        } catch (Exception e) {
                                            // This will catch any exception, because they are all descended from Exception
                                            System.out.println("Error " + e.getMessage());
                                            String currentDateTimeString2 = java.text.DateFormat.getDateInstance().format(new Date());
                                            exercicio = new Exercicio(mFirebaseUser.getEmail(),retirartitulo, currentDateTimeString2, tempoTotal, retirarvelocidade , retirardistancia, caloriakm , (String) getText(R.string.treinoModerado),rota); //nao sei se esta a receber bem as calorias
                                            exercicioViewModel.insert(exercicio);
                                        }
                                    }
                                });
                                dialog.dismiss();
                                break;
                            case 2:
                                retirarTempo = tempoTotal * (float) 2;
                                tempocorrida = (float) tempoTotal/3600000;
                                h = (int) (tempoTotal / 3600000);
                                m = (float) (tempoTotal - h * 3600000) / 60000;

                                if(distancia.getText().toString().matches("")){
                                    retirardistancia = Float.valueOf(0);
                                    retirarvelocidade = Float.valueOf(0);;
                                }else{
                                    retirardistancia = Float.valueOf(distancia.getText().toString());
                                    retirarvelocidade = (float) retirardistancia/tempocorrida;
                                }
                                if(titulo.getText().toString().matches("")){
                                    retirartitulo = (String) getText(R.string.defaultTreinoGym);
                                } else{
                                    retirartitulo = titulo.getText().toString();
                                }

                                final Map<String, Object> posTreino3 = new HashMap<>();
                                posTreino3.put("Objetivo", tempoSemanal - retirarTempo);

                                db.collection("Users").document(mFirebaseUser.getEmail())
                                        .update(posTreino3)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });

                                cronometro.setBase(SystemClock.elapsedRealtime());
                                //intensidade = (String) getText(R.string.treinoLeve);
                                DocumentReference docRef3 = db.collection("Users").document(mFirebaseUser.getEmail());
                                docRef3.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String userPeso = documentSnapshot.getString("Peso");
                                        try {
                                            caloriasValor = (float) (Float.parseFloat(userPeso) * 0.0175 * retirarvelocidade);
                                            caloriakm = caloriasValor * m;
                                            String currentDateTimeString3 = java.text.DateFormat.getDateInstance().format(new Date());
                                            exercicio = new Exercicio(mFirebaseUser.getEmail(),retirartitulo, currentDateTimeString3, tempoTotal, retirarvelocidade , retirardistancia, caloriakm , (String) getText(R.string.treinoIntenso),rota); //nao sei se esta a receber bem as calorias
                                            exercicioViewModel.insert(exercicio);
                                        } catch (Exception e) {
                                            // This will catch any exception, because they are all descended from Exception
                                            String currentDateTimeString3 = java.text.DateFormat.getDateInstance().format(new Date());
                                            exercicio = new Exercicio(mFirebaseUser.getEmail(),retirartitulo, currentDateTimeString3, tempoTotal, retirarvelocidade , retirardistancia, caloriakm , (String) getText(R.string.treinoIntenso),rota); //nao sei se esta a receber bem as calorias
                                            exercicioViewModel.insert(exercicio);
                                        }
                                    }
                                });
                                dialog.dismiss();
                                break;
                        }
                    }
                });
                dialog.setCancelable(false);
                dialog.show();

            }
        });
        if (aFuncionar != false) {
            setNotificationButtonState(false, true, true);

        } else {
            setNotificationButtonState(true, false, false);
        }
        setMusicButtonState(true, false);
        setGuardarButtonState(false);

    }

    public void setNotificationButtonState(Boolean isRunEnable,
                                           Boolean isPauseEnable,
                                           Boolean isStopEnable) {
        iniciarCorrida.setEnabled(isRunEnable);
        pausarCorrida.setEnabled(isPauseEnable);
        acabarCorrida.setEnabled(isStopEnable);
    }

    public void setMusicButtonState(Boolean isPlaying,
                                    Boolean isStopEnable) {
        startMusica.setEnabled(isPlaying);
        stopMusica.setEnabled(isStopEnable);
    }

    public void setGuardarButtonState(Boolean podeGuardar) {
        btnTerminar.setEnabled(podeGuardar);
    }

    @Override
    public void onResume() {
        super.onResume();
        pauseOffset = 0;
    }

    @Override
    public void onStop() {
        super.onStop();
        pauseOffset = 0;
    }
}
