package com.example.trabalhofinal;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class ExercicioRepository {
    private ExercicioDao exercicioDao;
    private LiveData<List<Exercicio>> todosExercicios;

    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

    public ExercicioRepository(Application application) {
        ExercicioDatabase database = ExercicioDatabase.getInstance(application);
        exercicioDao = database.exercicioDao();
        todosExercicios = exercicioDao.getAll(mFirebaseUser.getEmail());
    }

    public void insert(Exercicio exercicio) {
        new InsertExercicioAsyncTask(exercicioDao).execute(exercicio);
    }

    public void update(Exercicio exercicio) {
        new UpdateExercicioAsyncTask(exercicioDao).execute(exercicio);
    }

    public void delete(Exercicio exercicio) {
        new DeleteExercicioAsyncTask(exercicioDao).execute(exercicio);
    }

    public void deleteAllNotes() {
        new DeleteAllExerciciosAsyncTask(exercicioDao).execute();
    }

    public LiveData<List<Exercicio>> getTodosExercicios() {
        return todosExercicios;
    }

    private static class InsertExercicioAsyncTask extends AsyncTask<Exercicio, Void, Void> {
        private ExercicioDao exercicioDao;

        private InsertExercicioAsyncTask(ExercicioDao exercicioDao) {
            this.exercicioDao = exercicioDao;
        }

        @Override
        protected Void doInBackground(Exercicio... exercicios) {
            exercicioDao.insert(exercicios[0]);
            return null;
        }
    }

    private static class UpdateExercicioAsyncTask extends AsyncTask<Exercicio, Void, Void> {
        private ExercicioDao exercicioDao;

        private UpdateExercicioAsyncTask(ExercicioDao exercicioDao) {
            this.exercicioDao = exercicioDao;
        }

        @Override
        protected Void doInBackground(Exercicio... exercicios) {
            exercicioDao.update(exercicios[0]);
            return null;
        }
    }

    private static class DeleteExercicioAsyncTask extends AsyncTask<Exercicio, Void, Void> {
        private ExercicioDao exercicioDao;

        private DeleteExercicioAsyncTask(ExercicioDao exercicioDao) {
            this.exercicioDao = exercicioDao;
        }

        @Override
        protected Void doInBackground(Exercicio... exercicios) {
            exercicioDao.delete(exercicios[0]);
            return null;
        }
    }

    private static class DeleteAllExerciciosAsyncTask extends AsyncTask<Void, Void, Void> {
        private ExercicioDao exercicioDao;

        private DeleteAllExerciciosAsyncTask(ExercicioDao exercicioDao) {
            this.exercicioDao = exercicioDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            exercicioDao.deleteAllExercicios();
            return null;
        }
    }
}
