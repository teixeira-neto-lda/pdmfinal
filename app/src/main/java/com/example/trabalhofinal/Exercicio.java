package com.example.trabalhofinal;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "Exercicios_table")
public class Exercicio {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @NonNull
    private String email;

    @NonNull
    private String tipo;

    @NonNull
    private String data;

    private float cornometro;

    private float velocidade;

    private float distancia;

    private float calorias;

    private String intensidade;

    private String rota;

    public Exercicio(@NonNull String email, @NonNull String tipo, @NonNull String data, float cornometro, float velocidade, float distancia, float calorias, String intensidade, String rota) {
        this.email = email;
        this.tipo = tipo;
        this.data = data;
        this.cornometro = cornometro;
        this.velocidade = velocidade;
        this.distancia = distancia;
        this.calorias = calorias;
        this.intensidade = intensidade;
        this.rota = rota;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getTipo() {
        return tipo;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    @NonNull
    public String getData() {
        return data;
    }

    public float getCornometro() {
        return cornometro;
    }

    public float getVelocidade() {
        return velocidade;
    }

    public float getDistancia() {
        return distancia;
    }

    public float getCalorias() {
        return calorias;
    }

    public String getIntensidade() {
        return intensidade;
    }

    public String getRota() {
        return rota;
    }
}
