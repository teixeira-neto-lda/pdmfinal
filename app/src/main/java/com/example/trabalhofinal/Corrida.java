package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class Corrida extends AppCompatActivity implements Click{

    private boolean tabletMode;
    private static final int REQUEST_LOCATION_PERMISSION = 101;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location myLocation;
    private boolean accao = false;
    MapFragment mapa;
    ArrayList<LatLng> array = new ArrayList();
    String s = "";
    BroadcastReceiver broadcastReceiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corrida);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getDeviceLocation();

        if(findViewById(R.id.corridaframe) == null){
            tabletMode = true;
        }else{
            tabletMode = false;
        }

        if(!tabletMode) {
            CorridaFragment corrida = new CorridaFragment();
            guardaArray();

      /*  if(aFuncionar){
            keepData();
        }*/

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();

            transaction.replace(R.id.corridaframe, corrida);

            transaction.commit();
        }

    }


    @Override
    public void abreMapa(boolean accao) {
         //mapa= new MapFragment();
        if(!tabletMode) {
            mapa = new MapFragment();
        } else {
            mapa = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentMapa);
        }

        if(accao == true){

            Bundle b= new Bundle();
            b.putBoolean("Estado", accao);

            //aqui recebe o cornometro e os dados

            guardaArray();

            mapa.setArguments(b);


        }

        if(getDeviceLocation() != null){
            mapa.setLat(getDeviceLocation().getLatitude());
            mapa.setLng(getDeviceLocation().getLongitude());
        }

        if(!tabletMode) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();

            transaction.replace(R.id.corridaframe,mapa);
            transaction.addToBackStack(null);

            transaction.commit();
        }else{//Aqui estou a reabrir o mapa e tenho que por a atualizar

            Toast.makeText(this,"BORRO", Toast.LENGTH_LONG).show();

            mapa.recebeEstado(accao);
        }



    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful()) {
                                myLocation = task.getResult();
                                if (myLocation != null) {
                                    //abreMapa();
                                } else {
                                    mLocationRequest = new LocationRequest();
                                    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                    mLocationRequest.setInterval(5000);
                                    mLocationRequest.setFastestInterval(5000);
                                    mLocationCallback = new LocationCallback() {
                                        @Override
                                        public void onLocationResult(LocationResult locationResult) {
                                            super.onLocationResult(locationResult);
                                            if (locationResult == null) {
                                                return;
                                            }
                                            myLocation = locationResult.getLastLocation();
                                        }
                                    };
                                    fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getText(R.string.erroGPS), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(this, getText(R.string.erroPermissoes), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public Location getDeviceLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }else {

            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        myLocation = task.getResult();
                        if (myLocation != null) {
                            //Toast.makeText(getApplicationContext(), getText(R.string.sucessoGPS), Toast.LENGTH_SHORT).show();
                        } else {
                            mLocationRequest = new LocationRequest();
                            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            mLocationRequest.setInterval(5000);
                            mLocationRequest.setFastestInterval(5000);
                            mLocationCallback = new LocationCallback() {
                                @Override
                                public void onLocationResult(LocationResult locationResult) {
                                    super.onLocationResult(locationResult);
                                    if (locationResult == null) {
                                        return;
                                    }
                                    myLocation = locationResult.getLastLocation();
                                }
                            };
                            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getText(R.string.erroGPS), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    return myLocation;
    }


/*    public void startLocationUpdates() {
        //verificar premissoes
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(5000);
            mLocationRequest.setFastestInterval(5000);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    if (locationResult == null) {
                        return;
                    }
                    myLocation = locationResult.getLastLocation();
                }
            };
            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }
    }

    public void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    public void passarAccao (boolean b){


    }*/


    public void startService(View view) {
        startService(new Intent(this, SoundService.class));
    }

    public void stopService(View view) {

        stopService(new Intent(this, SoundService.class));
    }

    public void startServiceMapa(View view) {
        startService(new Intent(this, MapaService.class));
    }


    public void stopServiceMapa(View view) {

        stopService(new Intent(this, MapaService.class));
    }

    public void guardaArray(){

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LatLng latLng = new LatLng(intent.getExtras().getDouble("Latitude"), intent.getExtras().getDouble("Longitude"));

                s = s + latLng.latitude + "," + latLng.longitude + ";";
                array.add(latLng);

                Intent tentativa = new Intent("passaLista");
                tentativa.putExtra("lista", array);
                tentativa.putExtra("string", s);
                sendBroadcast(tentativa);

                //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();

            }
        };
        this.registerReceiver(broadcastReceiver, new IntentFilter("location"));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.unregisterReceiver(broadcastReceiver);
    }

}
