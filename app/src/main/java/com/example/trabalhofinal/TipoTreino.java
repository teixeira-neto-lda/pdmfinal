package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TipoTreino extends AppCompatActivity {

    Button corrida, gym;
    String APIKey = "b6a7acd9bdc2c2263015a88d5dc2b9eb";
    AlertDialog.Builder builder;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location myLocation;
    private static final int REQUEST_LOCATION_PERMISSION = 101;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

    public void setButtonState(Boolean corridaEnable,
                                    Boolean gymEnable) {
        corrida.setEnabled(corridaEnable);
        gym.setEnabled(gymEnable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_treino);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getDeviceLocation();


        builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(R.string.btnDialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setCancelable(true);


        corrida = findViewById(R.id.btn_corrida);
        corrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Corrida.class));
            }
        });
        gym = findViewById(R.id.btn_gym);
        gym.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Gym.class));
            }
        });

        setButtonState(false, false);

        new CountDownTimer(2000, 10) { //Set Timer for 5 seconds
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {

                DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        String userPeso = documentSnapshot.getString("Peso");
                        String userAltura = documentSnapshot.getString("Altura");
                        String userIdade = documentSnapshot.getString("Idade");

                        if(userPeso == null || userAltura == null || userIdade == null){
                            setButtonState(true, false);
                            Toast.makeText(getApplicationContext(), getText(R.string.naoPodeGym),Toast.LENGTH_LONG).show();
                        }else{
                            setButtonState(true, true);
                        }
                    }
                });

            }
        }.start();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful()) {
                                myLocation = task.getResult();
                                if (myLocation != null) {
                                    //abreMapa();
                                } else {
                                    mLocationRequest = new LocationRequest();
                                    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                    mLocationRequest.setInterval(5000);
                                    mLocationRequest.setFastestInterval(5000);
                                    mLocationCallback = new LocationCallback() {
                                        @Override
                                        public void onLocationResult(LocationResult locationResult) {
                                            super.onLocationResult(locationResult);
                                            if (locationResult == null) {
                                                return;
                                            }
                                            myLocation = locationResult.getLastLocation();
                                        }
                                    };
                                    fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getText(R.string.erroGPS), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    builder.setMessage(getText(R.string.erroPermissoes));
                    builder.show();
                }
                break;
        }
    }

    public Location getDeviceLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {

            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        myLocation = task.getResult();
                        if (myLocation != null) {
                            Toast.makeText(getApplicationContext(), getText(R.string.sucessoGPS), Toast.LENGTH_SHORT).show();

                            String lat = String.valueOf(myLocation.getLatitude());
                            String lon = String.valueOf(myLocation.getLongitude());
                            final Retrofit retrofit = new Retrofit.Builder()
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .baseUrl("https://api.openweathermap.org/")
                                    .build();
                            final Openweather apiService = retrofit.create(Openweather.class);
                            apiService.getWeatherDetails(lat, lon, APIKey, "metric").enqueue(new Callback<WeatherResponse>() {
                                @Override
                                public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                                    WeatherResponse points = response.body();
                                    switch (points.weather.get(0).main) {
                                        case "Thunderstorm":
                                        case "Drizzle":
                                        case "Rain":
                                        case "Snow":
                                        case "Mist":
                                            builder.setTitle(points.main.temp + "ºC");
                                            builder.setIcon(R.mipmap.chuva_foreground);
                                            builder.setMessage(getText(R.string.mistMessage));
                                            builder.show();
                                            break;
                                        case "Clear":
                                        case "Clouds":
                                            builder.setTitle(points.main.temp + "ºC");
                                            builder.setIcon(R.mipmap.sol_foreground);
                                            builder.setMessage(getText(R.string.cloudsMessage));
                                            builder.show();
                                            break;
                                        default:
                                            builder.setMessage(getText(R.string.defaultMessage));
                                            builder.show();
                                            break;
                                    }
                                }

                                @Override
                                public void onFailure(Call<WeatherResponse> call, Throwable t) {
                                    builder.setMessage(getText(R.string.defaultMessage));
                                    builder.show();
                                }
                            });
                        } else {
                            mLocationRequest = new LocationRequest();
                            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            mLocationRequest.setInterval(5000);
                            mLocationRequest.setFastestInterval(5000);
                            mLocationCallback = new LocationCallback() {
                                @Override
                                public void onLocationResult(LocationResult locationResult) {
                                    super.onLocationResult(locationResult);
                                    if (locationResult == null) {
                                        return;
                                    }
                                    myLocation = locationResult.getLastLocation();
                                }
                            };
                            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getText(R.string.erroGPS), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        return myLocation;
    }
}
