package com.example.trabalhofinal;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class TreinoHistorico extends AppCompatActivity implements ClickHist{

    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_TIPO = "EXTRA_TIPO";
    public static final String EXTRA_DATA = "EXTRA_DATA";
    public static final String EXTRA_CORNOMETRO = "EXTRA_CORNOMETRO";
    public static final String EXTRA_VELOCIDADE = "EXTRA_VELOCIDADE";
    public static final String EXTRA_DISTANCIA = "EXTRA_DISTANCIA";
    public static final String EXTRA_CALORIAS = "EXTRA_CALORIAS";
    public static final String EXTRA_ROTA = "EXTRA_ROTA";
    public static final String EXTRA_EMAIL = "EXTRA_EMAIL";
    public static final String DETALHES = "DETALHES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treino_historico);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //detalhesTreino();


        DetalhesFragment detalhesFragment = new DetalhesFragment();
        Intent intent = getIntent();

        Bundle bundle2 = intent.getExtras();
        //Toast.makeText(this, bundle2.getString(EXTRA_DATA), Toast.LENGTH_LONG).show();
        if(intent.hasExtra(EXTRA_ID)){
            Bundle b = new Bundle();
            b.putInt(DetalhesFragment.EXTRA_ID_DETALHES, bundle2.getInt(EXTRA_ID));
            b.putString(DetalhesFragment.EXTRA_TIPO_DETALHES, bundle2.getString(EXTRA_TIPO));
            b.putString(DetalhesFragment.EXTRA_DATA_DETALHES, bundle2.getString(EXTRA_DATA));
            b.putFloat(DetalhesFragment.EXTRA_CORNOMETRO_DETALHES, bundle2.getFloat(EXTRA_CORNOMETRO));
            b.putFloat(DetalhesFragment.EXTRA_VELOCIDADE_DETALHES, bundle2.getFloat(EXTRA_VELOCIDADE));
            b.putFloat(DetalhesFragment.EXTRA_DISTANCIA_DETALHES, bundle2.getFloat(EXTRA_DISTANCIA));
            b.putFloat(DetalhesFragment.EXTRA_CALORIAS_DETALHES, bundle2.getFloat(EXTRA_CALORIAS));
            b.putString(DetalhesFragment.EXTRA_ROTA_DETALHES, bundle2.getString(EXTRA_ROTA));
            detalhesFragment.setArguments(b);

        }

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(R.id.framelayout,detalhesFragment);
        //transaction.addToBackStack(null);

        transaction.commit();
    }

    @Override
    public void aberturaMapa(String rota) {
        MapaDetalhesFragment mapa = new MapaDetalhesFragment();

        Bundle b= new Bundle();
        b.putString("Detalhes", rota);
        mapa.setArguments(b);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(R.id.framelayout,mapa);
        transaction.addToBackStack(null);

        transaction.commit();
    }
}
