package com.example.trabalhofinal;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

public class SoundService extends Service {
    MediaPlayer player;

    public SoundService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public int onStartCommand(Intent intent,int flags, int startId) {
        Toast.makeText(this, getText(R.string.musicaComeca), Toast.LENGTH_LONG).show();
        player = MediaPlayer.create(this, R.raw.file);
        player.setLooping(true); // Set looping
        player.start();
        return START_STICKY;

    }
    @Override
    public void onDestroy() {
        Toast.makeText(this, getText(R.string.musicaAcaba), Toast.LENGTH_LONG).show();
        player.stop();
    }
}
