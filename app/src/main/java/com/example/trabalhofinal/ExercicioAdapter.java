package com.example.trabalhofinal;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class ExercicioAdapter extends RecyclerView.Adapter<ExercicioAdapter.ExercicioHolder> {

    private List<Exercicio> exercicios = new ArrayList<>();
    private onItemClickListener monExercicioListener;
    private ClickHist clickItemListener;
    private onItemClickListener listener;

   /* public ExercicioAdapter(List<Exercicio> exercicios, onItemClickListener monExercicioListener){
        this.exercicios = exercicios;
        this.monExercicioListener = monExercicioListener;
    }*/

    @NonNull
    @Override
    public ExercicioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.historico_item, parent, false);

        return new ExercicioHolder(itemView, monExercicioListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExercicioHolder holder, int position) {
        Exercicio currentExercicio = exercicios.get(position);
        holder.data.setText(currentExercicio.getData());
        holder.tipoTreino.setText(currentExercicio.getTipo());
        holder.nivelIntensidade.setText(currentExercicio.getIntensidade());

    }

    @Override
    public int getItemCount() {
        return exercicios.size();
    }

    public void setExercicios(List<Exercicio> exercicios){
        this.exercicios = exercicios;
        notifyDataSetChanged(); //vai ser replace
    }

    public Exercicio getExercicioAt(int position){
        return  exercicios.get(position);
    }

    class ExercicioHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView data;
        private TextView tipoTreino;
        private TextView nivelIntensidade;
        onItemClickListener onExercicioListener;

        public ExercicioHolder(View itemView, onItemClickListener onExercicioListener) {
            super(itemView);
            data = itemView.findViewById(R.id.text_data);
            tipoTreino = itemView.findViewById(R.id.text_tipoTreino);
            nivelIntensidade = itemView.findViewById(R.id.intensidadeTxt);

            this.onExercicioListener = onExercicioListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(exercicios.get(getAdapterPosition()));
        }
    }
/*
    public interface OnExercicioListener{
        void onExercicioClick(int position);
    }*/

    public interface onItemClickListener {
        void onItemClick(Exercicio exercicio);
    }

    public void setOnItemClickListener(onItemClickListener listener) {
        this.listener = listener;
    }
}
