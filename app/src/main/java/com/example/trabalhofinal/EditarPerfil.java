package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class EditarPerfil extends AppCompatActivity {

    EditText pesoImput, alturaImput, idadeImput;
    Button btnGuardar;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        pesoImput = findViewById(R.id.pesoImput);
        alturaImput = findViewById(R.id.alturaImput);
        idadeImput = findViewById(R.id.idadeImput);


        btnGuardar = findViewById(R.id.btnGuardar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

                final String peso = pesoImput.getText().toString().trim();
                final String altura = alturaImput.getText().toString().trim();
                final String idade = idadeImput.getText().toString().trim();

                final Map<String, Object> user = new HashMap<>();
                user.put("Peso", peso);
                user.put("Altura", altura);
                user.put("Idade", idade);


                if (TextUtils.isEmpty(peso)) {
                    pesoImput.setError(getText(R.string.erroPesoRequirido));
                    return;
                }

                if (TextUtils.isEmpty(altura)) {
                    alturaImput.setError(getText(R.string.erroAlturaRequirido));
                    return;
                }

                if (TextUtils.isEmpty(idade)) {
                    idadeImput.setError(getText(R.string.erroIdadeRequirido));
                    return;
                }

                db.collection("Users").document(mFirebaseUser.getEmail())
                        .update(user)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                //Log.d(TAG, "DocumentSnapshot successfully written!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(EditarPerfil.this, "Erro!!" + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });

                startActivity(new Intent(getApplicationContext(),Perfil.class));

            }
        });



    }
}
