package com.example.trabalhofinal;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final int REQUEST_LOCATION_PERMISSION = 100;

    private Context context;
    private GoogleMap mMap;
    private float zoom = 16;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location myLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    double lat, lng;
    private boolean semUpdates = true;
    private Marker currentLocationMarker;
    private LatLng latLngprevious;

    private Click clickListener;

    public MapFragment() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //setContentView(R.layout.fragment_map);
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(v.getContext());
       /* try {
            if (getArguments().getBoolean("Detalhes") == true) {

                Toast.makeText(getContext(), "OLA OLHA EU CA ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("Error " + e.getMessage());
        }*/

        try {
            if (getArguments().containsKey("Estado")) {
                if (getArguments().getBoolean("Estado") == true) {
                    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {

                            ArrayList<LatLng> array = intent.getExtras().getParcelableArrayList("lista");

                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(array.get(array.size() - 1).latitude, array.get(array.size() - 1).longitude), 19));
                            //showMarker(latlong);
                            if (currentLocationMarker == null) {
                                currentLocationMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(array.get(array.size() - 1)));
                            } else {
                                MarkerAnimation.animateMarkerToGB(currentLocationMarker, array.get(array.size() - 1), new LatLngInterpolator.Spherical());
                                PolylineOptions polyLineOptions = new PolylineOptions();
                                polyLineOptions.add(array.get(array.size() - 2), array.get(array.size() - 1));
                                polyLineOptions.width(20);
                                polyLineOptions.color(Color.BLUE);
                                mMap.addPolyline(polyLineOptions);
                            }
                        }
                    };
                    getActivity().registerReceiver(broadcastReceiver, new IntentFilter("passaLista"));

                } else {
                    Toast.makeText(getContext(), getText(R.string.erro), Toast.LENGTH_SHORT).show();
                    ((Corrida) getActivity()).stopServiceMapa(v);
                }
            }
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("Error " + e.getMessage());
        }


        return v;
    }

 /*   public void onLocationChanged (Location location){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 19));
        showMarker(myLocation);

    }*/


    public void recebeEstado(boolean estado) {
//        Toast.makeText(getActivity(),"HEY",Toast.LENGTH_LONG).show();
        if (estado == true) {
            BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    ArrayList<LatLng> array = intent.getExtras().getParcelableArrayList("lista");

                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(array.get(array.size() - 1).latitude, array.get(array.size() - 1).longitude), 19));
                    //showMarker(latlong);
                    if (currentLocationMarker == null) {
                        currentLocationMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(array.get(array.size() - 1)));
                    } else {
                        MarkerAnimation.animateMarkerToGB(currentLocationMarker, array.get(array.size() - 1), new LatLngInterpolator.Spherical());
                        PolylineOptions polyLineOptions = new PolylineOptions();
                        polyLineOptions.add(array.get(array.size() - 2), array.get(array.size() - 1));
                        polyLineOptions.width(20);
                        polyLineOptions.color(Color.BLUE);
                        mMap.addPolyline(polyLineOptions);
                    }
                }
            };
            getActivity().registerReceiver(broadcastReceiver, new IntentFilter("passaLista"));

            semUpdates = false;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (lat != 0.0 && lng != 0.0) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom));
            mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("YOU"));
        } else {
            LatLng lousada = new LatLng(41.2775, -8.2837);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lousada, zoom));
            mMap.addMarker(new MarkerOptions().position(lousada).title("Marker in Lousada"));

        }
    }

    public void setLat(double lat) {

        this.lat = lat;

    }

    public void setLng(double lng) {

        this.lng = lng;
    }


/*    private void showMarker(@NonNull LatLng latLng) {

        //currentLocationMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng));
        if (currentLocationMarker == null) {
            currentLocationMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng));
            latLngprevious = latLng;
            //currentLocationMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng));
        } else {
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());
            currentLocationMarker.setPosition(latLng);

            PolylineOptions polyLineOptions = new PolylineOptions();
            polyLineOptions.add(latLngprevious, latLng);
            polyLineOptions.width(20);
            polyLineOptions.color(Color.BLUE);
            mMap.addPolyline(polyLineOptions);

            latLngprevious = latLng;
        }
    }*/


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map_options, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.normal_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.hybrid_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.terrain_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            case R.id.satellite_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (semUpdates = false) {
            if (fusedLocationProviderClient != null) {
                //fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
            }
        }
    }

}
