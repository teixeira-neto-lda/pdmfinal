package com.example.trabalhofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registo extends AppCompatActivity {

    EditText nomeReg, emailReg, passwordReg;
    Button btnRegisto;
    TextView loginLink;
    FirebaseAuth fAuth;
    ProgressBar progressBarReg;
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registo);

        nomeReg = findViewById(R.id.registoNome);
        emailReg = findViewById(R.id.registoEmail);
        passwordReg = findViewById(R.id.registoPass);
        btnRegisto = findViewById(R.id.btnRegistar);
        loginLink = findViewById(R.id.linkEntrar);

        fAuth = FirebaseAuth.getInstance();

        progressBarReg = findViewById(R.id.progressBarRegisto);

        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }

        btnRegisto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nome = nomeReg.getText().toString().trim();
                final Map<String, Object> user = new HashMap<>();
                user.put("Nome", nome);
                user.put("Objetivo", 9000000);
                final String email = emailReg.getText().toString().trim();
                String password = passwordReg.getText().toString().trim();

                if(TextUtils.isEmpty(nome)){
                    nomeReg.setError(getText(R.string.semNomeInserido));
                    return;
                }

                if(TextUtils.isEmpty(email)){
                    emailReg.setError(getText(R.string.semEmailInserido));
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    passwordReg.setError(getText(R.string.semPasswordInserido));
                    return;
                }

                if(password.length() < 8){
                    passwordReg.setError(getText(R.string.PasswordTamanho));
                    return;
                }

                progressBarReg.setVisibility(View.VISIBLE);

                //registar o utilizador na firebase

                fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //Toast.makeText(Registo.this, "Utilizador criado com sucesso!", Toast.LENGTH_SHORT).show();
                            db.collection("Users").document(email)
                                    .set(user)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            //Log.d(TAG, "DocumentSnapshot successfully written!");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(Registo.this, "Erro!!" + e.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        }else{
                            Toast.makeText(Registo.this, "Erro!!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBarReg.setVisibility(View.GONE);
                        }
                    }
                });

            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Login.class));
            }
        });
    }
}
