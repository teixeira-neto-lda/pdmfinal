package com.example.trabalhofinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class Manual extends AppCompatActivity {

    ViewPager viewPager;
    DemoAdapter adapter;
    List<Model> models;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        models = new ArrayList<>();
        models.add(new Model(R.mipmap.homepage_foreground,"Esta é a homepage apresentada ao utilizador mal ele faça login." + "\nExiste um botao de iniciar treino, um para visitar o historico e tambem é possivel aceder as preferencias da aplicação"
        + "\nTemos depois o floating button que permite o utilizador aceder ao seu perfil" + "\n\n\n\nDeslize para ver"));
        models.add(new Model(R.mipmap.perfilnot_foreground,"Mal efetue o registo o utilizador recebe esta notificação para que preencha os dados do seu perfil de maneira a que possa usufruir de todas as funcionalidades"));
        models.add(new Model(R.mipmap.perfilvazio_foreground,"O utilizador tem que inserir o seu peso, a sua altura e a idade"));
        models.add(new Model(R.mipmap.perfilcompleto_foreground,"Caso ele insira os dados este é o aspecto que o seu perfil deve assumir"));
        models.add(new Model(R.mipmap.perfilaltdados_foreground,"Se o utilizador quiser alterar os seus dados e novamente pedido todos os dados"));
        models.add(new Model(R.mipmap.preferencias_foreground,"Se o user for as preferencias pode desativar as notificações bem como enviar-mos um feedback"));
        models.add(new Model(R.mipmap.dialog_foreground,"Quando o utilizador selecciona a opção de iniciar atividade e apresentado um dialog com a metereologia de onde este se encotra e uma recomendação"));
        models.add(new Model(R.mipmap.gyminativo_foreground,"Quando o utilizador nao insere os restantes dados do registo nao lhe é permitido aceder ao treino de ginasio"));
        models.add(new Model(R.mipmap.corridaexterior_foreground,"Se o user seleccionar a corrida exterior aparece este layout, quando o user inicia a corrida os dados começam a contar, o user pode meter em pausa a atividade ou acabar a atividade"
        + "\nÉ possivel tambem ao utilizador iniciar e para a musica caso pretenda"));
        models.add(new Model(R.mipmap.mapa_foreground,"O user pode acompanhar o seu progresso no mapa tem tambem a disposição alguma configurações para o mapa"));
        models.add(new Model(R.mipmap.fimcorridaexterior_foreground,"Quando o utilizador clica no botao de terminar corrida é lhe apresentado uma escala de esforço, quando o utilizar clica no nivel de intensidade que pretende, sendo depois os dados guardados na base de dados local"));
        models.add(new Model(R.mipmap.gymfim_foreground, "No treino de ginasio o utilizador carrega no botao iniciar e o cronometro começa a contar bem, tendo igualmente um serviço de musica se pretender, quando finaliza"
        + "a sua atividades insere os dados que sao pedidos e no final pressiona o botão guardar onde é pedido que qualifique o seu esforço com base na escala de perceção de esforço sendo depois os valores guardados na base de dados"));
        models.add(new Model(R.mipmap.historico_foreground,"Quando o user seleciona o botao de historico sao apresentados todos os exercicios que este efetou"));
        models.add(new Model(R.mipmap.elimhistorico_foreground,"Ao utilizador e permitido eliminar os exercicios que quiser fazendo swipe para qualquer um dos lados e clicando posteriormente no botao de aceitar"));
        models.add(new Model(R.mipmap.notisemanal_foreground,"Quando o user atinge o objetivo semanal é lhe apresentada esta notificação para informar o mesmo"));

        adapter = new DemoAdapter(models,this);
        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
    }
}
