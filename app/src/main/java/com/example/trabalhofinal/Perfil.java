package com.example.trabalhofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class Perfil extends AppCompatActivity {

    public static final int PERFIL_ACTIVITY_REQUEST_CODE = 1;

    TextView peso, altura, idade, nome, tempoRestantText;
    Button editarDados;
    ProgressBar progressBar;
    int valorProgresso = 0;
    float minutosfalta = 0;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

        nome = findViewById(R.id.nomePerfil);
        peso = findViewById(R.id.pesoValor);
        altura = findViewById(R.id.alturaValor);
        idade = findViewById(R.id.idadeValor);
        progressBar = findViewById(R.id.progressBarObjetivo);
        tempoRestantText = findViewById(R.id.textViewProgresso);

        DocumentReference docRef = db.collection("Users").document(mFirebaseUser.getEmail());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String userNome = documentSnapshot.getString("Nome");
                String userPeso = documentSnapshot.getString("Peso");
                String userAltura = documentSnapshot.getString("Altura");
                String userIdade = documentSnapshot.getString("Idade");
                long userObjetivo = documentSnapshot.getLong("Objetivo");

                nome.setText(userNome);
                if(userPeso != null){
                    peso.setText(userPeso + " Kg");
                }
                if(userAltura != null){
                    altura.setText(userAltura + " m");
                }
                if(userIdade != null){
                    idade.setText(userIdade + " anos");
                }

                valorProgresso = (int) (userObjetivo * 100)/9000000;

                minutosfalta =  150 - ((float)userObjetivo/60000);

                tempoRestantText.setText(String.format("%.1f",minutosfalta)+" min / 150 min");

                progressBar.setProgress(100 - valorProgresso);

            }
        });


        editarDados = findViewById(R.id.btnEditarDados);
        editarDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Perfil.this, EditarPerfil.class);
                startActivityForResult(intent, PERFIL_ACTIVITY_REQUEST_CODE);
            }
        });
    }
}
